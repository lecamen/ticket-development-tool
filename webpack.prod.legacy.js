const { merge } = require('webpack-merge')
const common = require('./webpack.common')

 module.exports = merge(common, {
    mode: 'production',
    optimization: {
        minimize: true
    },
    output: {
        environment: {
            arrowFunction: false,
            module: false,
            const: false
        }
    },
    module: {
        rules: [
            {
                test: /\.m?js$/,
                exclude: /node_modules/,
                use: {
                    loader: "babel-loader",
                    options: {
                        presets: [
                            [
                                "@babel/preset-env",
                                {
                                    targets: {
                                        esmodules: false
                                    }
                                }
                            ]
                        ]
                    }
                }
            }
        ]
    }
 })