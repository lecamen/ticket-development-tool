const path = require('path');
const fs = require('fs');

const pattern = /(^|\/)\.[^\/\.]/g;
const alphabets = 'abcdefghijklmnopqrstuvwxyz'.split('');

const lifecycle = process.env.npm_lifecycle_event

let config = {}
let entries = {}

const variants = () => {

    const directoryFullPath = path.resolve('src', 'variants');

    for ( const dirName of fs.readdirSync( directoryFullPath ) ) {

        if ( !(pattern).test( dirName ) && !path.extname( dirName ) && alphabets.indexOf( dirName.toLowerCase() ) !== -1 && !dirName.includes('template')) {

            try {

                const variantDirectoryFullPath = path.resolve('src', 'variants', dirName.toLowerCase() )

                config = {
                    ...require( './' + path.join('src', 'variants', dirName.toLowerCase(), 'config'))
                }

                config.VERSION = ( 100 * process.pid).toString(36)

                for ( const variantDirectory of fs.readdirSync( variantDirectoryFullPath ) ) {

                    if ( path.extname( variantDirectory ) ) {

                        const outputFilename = (path.join(config.DOMAIN_NAME.toLowerCase(), config.NAME.replace(/([^0-9a-zA-Z])/g, '-'), `[${ config.ID }-${ config.VARIANT }-${ config.PREFIX }][${ lifecycle }][${ variantDirectory.replace('.js', '') }]` + ( lifecycle === 'dev' ? '' : `[V-${ config.VERSION }]` ))).toLowerCase().trim().replace(/\s/g, '-')
                        const runFilename = ( './' + path.join('src', 'variants', config.VARIANT, variantDirectory )).toLowerCase().replace(/ /g, '-')

                        entries[outputFilename] = runFilename

                    }

                }

            } catch {

                // console.log('Warning! directory is empty: ')

            }

        }

    }

    console.log({
        config: {
            ID: config.ID,
            DOMAIN_NAME: config.DOMAIN_NAME
        },
        entries: entries
    })

    return {
        config: config,
        entries: entries
    }

}

const experience = variants()

exports.config = experience.config

exports.entries = experience.entries