# Ticket Development Tool (Webpack)

This webpack template is easy to use. It's fully supported in all modern browsers and also legacy browser. Developer's can use latest ECMA script. ES6 up and ES5. 

**Template Output Builds**

- Output build support legacy browsers
- Multiple languages build
- Split chucks optimization ( Recommended for Google Optimized if the code is huge )
- Minified build in production

**Requirements** 

Download [NodeJs](https://nodejs.dev/download/)

Download [Git](https://git-scm.com/) ( if needed )

Alternative for macOS or linux 

Install [Brew](https://docs.brew.sh/Installation)

**Update the home brew package manager**

```jsx
brew update
```

**Install node.js using brew**

```jsx
brew install node
```

**To check node.js?**

```jsx
node -v
```

**Git Source**
[https://bitbucket.org/lecamen/ticket-development-tool/src](https://bitbucket.org/lecamen/ticket-development-tool/src)

**Download Master**

[https://bitbucket.org/lecamen/ticket-development-tool/get/master.zip](https://bitbucket.org/lecamen/ticket-development-tool/get/master.zip)

**How to download?**

First you need to remove "tdt" folder of exist. Copy this command to your terminal or cmd

**Windows**

```jsx
rm -r --force tdt
```

**Linux**

```jsx
sudo rm -r --force tdt
```

macOS

```jsx
sudo rm -r tdt
```

Copy this command to your terminal or cmd. *Make sure it executed in root directory.*

```jsx

git clone https://lecamen@bitbucket.org/lecamen/ticket-development-tool.git tdt
```

From the root branch change to "tdt" directory.

```jsx
cd tdt 
```

Then remove the ".git" (hidden) using this command. *Make sure it executed in "tdt" directory.*

**Windows**

```jsx
rm -r --force .git
```

**Linux**

```jsx
sudo rm -r --force .git
```

macOS

```jsx
sudo rm -r .git
```

*Don't moved all files to root. The tool should separate from "dist" folder ( generated/build  )*


---

# How to use?

**How to install dependency**

In terminal or cmd type "npm install" to install all dependency

```jsx
npm install
```

---

**How to add variant**

Go to "variants" folder and copy "_template". After copied, rename it on what variant you working on.

---

**How to add language**

In variant "b", "c" and etc... **"all.js"** file contains all languages.

**all.js** is default where we can develop for all languages.

**If need separate language**

Create each language as required. Also it's required to add language json file to "lang" folder
**en.js** is a separate English only
**nl.js** is a separate Dutch only

You can also add more language by also adding json file to "lang" folder and import it to the variant "x" file separated by language.

---

**How to configure experiment**

Go to "b" or "c" and etc.. then "config" folder and update the config file with ticket details. See description.

**VERSION**

Version control and it a part of webpack loader. DON'T CHANGE

**ID**

Provide ticket NUMBER or ID

**VARIANT**

Ticket variation reference. Default value is 'b'

**NAME**

Provide ticket name or title and ticket ID should be excluded

**DOMAIN_NAME**
Provide client domain name.

This is required if  need amazon s3 assets hosting.

```jsx
https://ta-client-assets.s3.amazonaws.com/[domain-name]/[ticket-id]/[filename.svg]'
```

**READY**

Run experiment IMMEDIATELY or READY

If TRUE, expermement will load when document is ready

If FALSE, expermement will load immediately

**ANTI_FLICKER**

Run experiment with Anti-Flicker default value is 0 second delay. Anti-Flicker will load white page before showing the variant.

**FORCE_LANGUAGE**

Run this selected language. This only work if no default language found.

**PREFIX**

Ticket Build Prefix, *Use for variant build e.g color blue or orange, small or big. Default is "cro"*

```jsx
[1248-b-prefix][prod-legacy][de][v-668].js

[1248-b-cro][prod-legacy][de][v-668].js

[1248-b-blue][prod-legacy][de][v-668].js
[1248-b-orange][prod-legacy][de][v-668].js
```

**TARGET_PATH**

Add selected URL for Load, Ready or SPA Target

**EXCLUDE_PATH**

Add selected URL to exclude Load, Ready or SPA Exclude Target

---


**How to make application**

In variant "b", "c" and etc... go to "app" folder. You can delete the all "sample" folder or rename it whatever app you what to make. You can also use "_app_template", just copy and rename it. 

Making application has format to follow. inside "app" folder create app with index.js file *( copy "_app_template" in easy way)*

```jsx
folder-app-name → index.js

// More files import to index
folder-app-name 
   → index.js
   → other.js
```

In make index.js this format must follow.

```jsx
export default {

	// Todo 

}
```

In make index.js this format must follow.

```jsx
export default {

	// Todo 

}
```

**Template lifecycle hooks**

---

**mounted()**

This will trigger Immediately or Ready. Or load Mutation Observer here

**updated()**

This will trigger when Push State and Popstate changed

Recommend to use for SPA site

**completed()**

This will trigger when XHR request completed

---

Use lifecycle hooks each which you need. 

```jsx
export default {

	mounted () {
        
        /*
        * Entry Point (Immediately or Ready)
        * This will trigger Immediately or Ready
        * Or load Mutation Observer here
        */

    },

    updated () {

        /*
        * Entry Point (Push State and Postate)
        * This will trigger when Push State, Postate changed and Resize
        * Recommend to use for SPA site
        */

    },

    completed () {

        /*
        * Entry Point (XHR) Complete
        * This will trigger when XHR request completed
        */

    }

}
```

Every application you made it will automatically imported and not need to import it


---


**Preset methods**
Can be found variants/[x]/core/index.js 

```jsx
// Native querySelector
this.querySelector

// Native querySelectorAll
this.querySelectorAll

// Native createElement
this.createElement

// Contains of all the configuration
this.config

// Return language translation
// lang/en.json or any language 
this.translation

// Return both variant and id 
// Set variant prefix
this.identity( prefix )

// Return test id
this.id

// Return variant e.g b, c and etc...
this.variant

// Return Amazon s3 full asset link
// set image filename with extension
// e.g "filename.svg"
this.s3( image )

// Return current language
this.language

// Add test style
this.addStyle()

// Add test id ( in class ) to html root 
this.addId()

// Remove test id useful for SPA
this.removeId()

// Remove styleid useful for SPA
this.removeStyle()

// More details
console.log(this)
```

---

# How to build?

**Production**

This will generated a minified script. Most compatible to testing tool

```jsx
npm run prod
```

**Production Modern**

This will generated a minified script. Recommended for inline setup ( ES6 and up)

```jsx
npm run prod-modern
```

**Production Legacy**

This will generated a minified script and support legacy browser

```jsx
npm run prod-legacy
```

**Development**

This will generated a development script. (Recommnded for Tampermonkey Used)

```jsx
npm run dev
```

# How to check experiment list running in the page and Production QA?

Open browser development tool then open console and type "**__TC_EXPERIENCE__**" in console. 

**__TC_EXPERIENCE__** contains the list of experiments loaded/running.

```jsx
__TC_EXPERIENCE__
```

---

In the list there are additional configuration listed. It has **VERSION** and **LIVE**

**VERSION**

Is a version build for Development and Production

**LIVE**

To check which build is current running. There are 3 values 'dev', 'qa' and 'true'

---

**Development**

**dev -** Will set local storage value of "dev". In this case live will automatically disabled. 

*To enable LIVE just removed the "tc[ID-VARIANT]" in the local storage.*

*Note: Don't send development code to client*

```jsx
localStorage.removeItem(`tc[ID-VARIANT]`)
```

**Production QA**

**qa -** It's live but and need a local storage data. Set value to local storage.

```jsx
localStorage.setItem(`tc[ID-VARIANT]`, 'qa')

// e.g

localStorage.setItem(`tc[1000-B]`, 'qa')
```

**Production**

**true -** Totally live using production code. This can disabled by adding local storage for "Development" or "Production QA". It's helpful for inline testing because Ghostery can't disabled it. To make Production run remove local storage "tc[ID-VARIANT]"

```jsx
localStorage.removeItem(`tc[ID-VARIANT]`)
```

---

# Output Build's

This generated script in "dist" folder

Inside "dist" folder choose a file link and copy link to your Tampermonkey

**Example Builds**

```jsx
// No version control in development but it still it's added to __TC_EXPERIENCE__
[1000-b][dev][all].js
[1000-b][dev][en].js
[1000-b][dev][nl].js

// Has version control
[1000-b][prod][all][v-kjcs].js
[1000-b][prod][en][v-kjcs].js
[1000-b][prod][nl][v-kjcs].js

[1000-b][prod-legacy][all][v-oerx].js
[1000-b][prod-legacy][en][v-oerx].js
[1000-b][prod-legacy][nl][v-oerx].js

// More details 
[ID-VARIANT][BUILD][LANG][v-VERSION].js
```

---

**Split Chunks**

Only use for Google Optimize. 

Uncomment the split chunks under optimization (**webpack.common.js**).

Update the size to make the code fit in Google Optimize.

```jsx
optimization: {
	splitChunks: {
        minSize: 12000,
        maxSize: 19000
	}
}
```

---

**Important**

Always push to GIT production build for QA

---

# Trackings

[Tracking Tool](https://www.notion.so/teamcroco/Code-Templates-f7e377742d3d4a6088cb7b6b2d965213)