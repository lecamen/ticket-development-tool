const path = require('path');
const fs = require('fs');

const pattern = /(^|\/)\.[^\/\.]/g;
const alphabets = 'abcdefghijklmnopqrstuvwxyz'.split('');

const appLoader = (content, request, callback) => {

    const directoryFullPath = path.resolve('src', 'variants');

    for ( const dirName of fs.readdirSync( directoryFullPath ) ) {

        let appDirectoryNames = [];
        let appImports = '';

        if ( !(pattern).test( dirName ) && !path.extname( dirName ) && alphabets.indexOf( dirName.toLowerCase() ) !== -1 && !dirName.includes('template')) {
            
            let appDirectoryFullPath;

            try {

                appDirectoryFullPath = path.resolve('src', 'variants', dirName.toLowerCase(), 'app')

                if ( content.includes('__IMPORTS__') && request.includes('core') ) {
             
                    for ( const appDirectoryName of fs.readdirSync( appDirectoryFullPath ) ) {

                        if ( !path.extname(appDirectoryName) && !appDirectoryName.includes('_')) {

                            const appName = ( appDirectoryName.replace(/\b\w/g, c => c.toUpperCase()) ).replace(/[^a-z0-9]/gi, '');

                            appDirectoryNames.push(`${ appName }`)

                            appImports += `import ${ appName } from '../app/${ appDirectoryName }' \n`

                        }

                    }

                    content = content.replace( /(\'__IMPORTS__\'|\"__IMPORTS__\")/g , appImports )

                    content = content.replace( /(\'__APPS__\'|\"__APPS__\")/g , appDirectoryNames )

                }

            } catch {

                // console.log('Warning! directory is empty: ', appDirectoryFullPath)

            }

        }


    }    

    callback(null, content)

}

module.exports = function (content) {

    const callback = this.async();
    const request = this.data.request

    appLoader(content, request, function ( error, result ) {

        if ( error ) { 
		
			return callback(err)

		}

        callback(null, result)
    })

}

module.exports.pitch = function (remainingRequest, precedingRequest, data) {

    data.request = remainingRequest

};