const versionLoader = (content, request, callback) => {

    content = content.replace( /(\'__VERSION__\'|\"__VERSION__\")/g, `'${ (100 * process.pid).toString(36) }'` )

    callback(null, content)

}

module.exports = function (content) {

    const callback = this.async();

    const request = this.data.request

    versionLoader(content, request, function ( error, result ) {

        if ( error ) { 
		
			return callback(err)

		}

        callback(null, result)
    })

}

module.exports.pitch = function (remainingRequest, precedingRequest, data) {

    data.request = remainingRequest

};