
const loaderUtils = require("loader-utils");
const css = require("css");
const path = require('path');

const cssLoader = (content, options, request, callback) => {

    const parsedCss = css.parse(content, { source: request });

    cssRuleUpdate(parsedCss.stylesheet.rules, options);

    callback(null, css.stringify(parsedCss, { compress: options.minify }))

}

function cssRuleUpdate(rules, options) {
    rules.forEach(rule => {
        if (rule.selectors) {
            rule.selectors.forEach((selector, index) => {
                if (
                    selector.startsWith("@")
                    || (options.exclude && options.exclude.test(selector))) {
                    return;
                }
                
                const variantsDir = rule.position.source.split('css')[0]

                const config = require( variantsDir + '/config')

                rule.selectors[index] = options.selector.trim() + `-${ config.ID }-${config.VARIANT} ` + selector;
                
            });
        }
        if (rule.rules) {
            cssRuleUpdate(rule.rules, options);
        }
    });
}

function getOptions(ctx) {
    
    const defaultOptions = {
        minify: ctx.mode === "production",
        selector: null,
        exclude: null,
        path: null
    };
    const options = loaderUtils.getOptions(ctx);
    const res = Object.assign(defaultOptions, options);

    if (typeof options.selector !== "string" || !options.selector.trim()) {
        throw new Error("Missing required option: selector")
    }
    
    res.selector = res.selector.trim() + " ";

    if (res.exclude && !(res.exclude instanceof RegExp)) {
        res.exclude = new RegExp(res.exclude);
    }

    return res;
}

module.exports = function(content) {

    const callback = this.async();

    const options = getOptions(this);

    const request = this.data.request;

    cssLoader(content, options, request, function ( error, result ) {

        if ( error ) { 
		
			return callback(err)

		}

        callback(null, result)
    })

}

module.exports.pitch = function (request, precedingRequest, data) {

    data.request = request;

};