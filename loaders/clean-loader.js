const cleanLoader = (content, callback) => {

    content = content.replace(/\\n[\s]+/g, '')
    content = content.replace(/\\t[\s]+/g, '')
    content = content.replace(/    /g, '')
    content = content.replace(/(\"use strict\"\;|\'use strict\'\;)/g, '')

    callback(null, content)

}

module.exports = function (content) {

    const callback = this.async();

    cleanLoader(content, function ( error, result ) {

        if ( error ) { 
		
			return callback(err)

		}

        callback(null, result)

    })

}