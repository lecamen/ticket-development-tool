let lifecycle = process.env.npm_lifecycle_event

const bootstrapLoader = (content, request, callback) => {

    lifecycle = lifecycle === 'dev' ? lifecycle : lifecycle === 'prod-qa' ? lifecycle : 'prod'
  
    content = content.replace( /(\'__BOOTSTRAP__\'|\"__BOOTSTRAP__\")/g , `import bootstrap from '../../bootstrap/${ lifecycle }' ` )

    callback(null, content)

}

module.exports = function (content) {

    const callback = this.async();

    const request = this.data.request

    bootstrapLoader(content, request, function ( error, result ) {

        if ( error ) { 
		
			return callback(err)

		}

        callback(null, result)
    })

}

module.exports.pitch = function (remainingRequest, precedingRequest, data) {

    data.request = remainingRequest

}