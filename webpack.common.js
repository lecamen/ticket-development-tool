const path = require('path')
const fs = require('fs')

const { CleanWebpackPlugin } = require('clean-webpack-plugin')

const { config, entries } = require('./webpack.experience');
const { resolve } = require('path');

module.exports = {
    entry: entries,
    output: {
        path: path.resolve(__dirname,'../', 'dist'),
		publicPath: 'https://ta-client-assets.s3.amazonaws.com'
    },
    module: {
        rules: [
            {
                test: /\.css$/,
                use: [
                    {
                        loader: "css-loader",
                        options: {
                            esModule: false,
                            importLoaders: 1
                        }
                    },
                    {
                       loader: 'css-prefix-loader',
                        options: {
                            selector: `.TCV`.toLowerCase(),
                            exclude: 'html',
                            minify: true
                        }
                    },
                ]
            },
            {
                test: /\.(png|svg|jpg|jpeg|gif|ico)$/i,
                loader: "file-loader",
                options: {
                    name: '/' + config.DOMAIN_NAME.toLowerCase() + '/' + config.ID + '/[name].[ext][query]',
                    emitFile: false,
					esModule: false
                }
            },
            {
                test: /\.js$/,
                use: [
                    {
                        loader: "bootstrap-loader"
                    },
                    {
                        loader: "app-loader"
                    },
                    {
                        loader: "clean-loader"
                    },
                    {
                        loader: "version-loader"
                    }
                ]
            }
        ],
    },
    optimization: {
        /*
		splitChunks: {
            minSize: 12000,
            maxSize: 19000
        }
		*/
    },
    plugins: [
        new CleanWebpackPlugin()
    ],
    resolveLoader: {
        modules: ['node_modules', path.resolve(__dirname, 'loaders')],
    }
};