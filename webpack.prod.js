const { merge } = require('webpack-merge')
const common = require('./webpack.common')

 module.exports = merge(common, {
    mode: 'production',
    output: {
        environment: {
            arrowFunction: false,
            module: false,
            const: false
        }
    },
    optimization: {
        minimize: true
    }
})