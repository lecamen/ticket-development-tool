import gaSend from "./google-analytics/conversion"

var toolConfig = {
    gaid: 'UA-107242044-1',
    options: {
		CLIENT_CODE: '77D',
		EXPERIMENT_NO: '1.0',
		VARIATION_NO: '1'
	}
};


gaSend(toolConfig, 'event', 'Pageview', true )

document.body.addEventListener('click', function(){

    gaSend(toolConfig, 'event', 'Clicks on benefit 1 ', false )

})