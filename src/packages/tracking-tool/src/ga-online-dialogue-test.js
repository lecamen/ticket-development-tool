import gaSend from "./google-analytics/online-dialogue"
 
var toolConfig = {
    gaid: 'UA-107242044-1',
    category: 'AB-test',
    testId: 'OD235',
    devices: 'DTM',
    page: 'Winkelwagen',
    action: ' Retourinformatie tonen',
    variant: 'B: Variant',
    targeting: '',
    testURL: ''
};
 
gaSend(toolConfig, 'pageview')

document.body.addEventListener('click', () => {
 
	gaSend(toolConfig, 'eventAction', false )

	// Extra
	gaSend(toolConfig, 'eventAction', 'eventLabel', false )

})