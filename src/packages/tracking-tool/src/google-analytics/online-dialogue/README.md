# Online Dialogue GA Tracking #

This method send data to Google Analytics ( Customized ).

---

**How do I get set up?**

```jsx

// Import from specific directory.

import gaSend from './[DIR]/google-analytics/online-dialogue';

// if not using node module 
// copy snippet from /[DIR]/google-analytic/online-dialogue/es5/google-analytic.es5.min.js

```

# Online Dialogue GA Tracking

Send data to google analytics Online Dialogue requirement. 

**Configurations**

```jsx
/*
* GA Config
*/
var toolConfig = {
    gaid: 'UA-202XXXX-XX',
	category: 'AB-test',
	testId: 'ODXXX',
	devices: 'DTM',
	page: 'Page Name',
	action: ' Page interaction',
	variant: 'B: Variant'
}
```

**Parameters**

```jsx
/*
* @config
* Tool configuration
*/

/*
* @toolConfig : Online Dialogue configuration
* @eventAction : e.g "pageview" or "Custom action event"
* @eventLabel : e.g "Custom label event"
* @Boolean : nonInteraction
*/

gaSend(toolConfig, eventAction, true)

gaSend(toolConfig, eventAction, eventLabel, false)

```

**Usage**

```jsx
/*
* GA Config
* See more config above
*/
var toolConfig = {
    gaid: 'UA-202XXXX-XX'
	...,
	...,
	...
}

/* GA Send */
function gaSend(o){ ... }
/* END - GA Send */

/*
* Sample click event
*/
document.body.addEventListener('click', function(){

	gaSend(toolConfig, 'eventAction', false )

	// Extra
	gaSend(toolConfig, 'eventAction', 'eventLabel', false )

})

/*
* Sample Page view
*/
gaSend(toolConfig, 'pageview', true)

```