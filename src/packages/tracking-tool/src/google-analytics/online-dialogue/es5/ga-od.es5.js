'use strict';

function gaSend(config) {

    var args = [].concat(Array.prototype.slice.call(arguments));

    var GAindex = 0,
        trackerIndex = 0;

    function gaTracker() {

        if (window[window['GoogleAnalyticsObject']] && window[window['GoogleAnalyticsObject']].create) {

            var trackers = window[window['GoogleAnalyticsObject']].getAll(),
                i = 0;

            for (i = 0; i < trackers.length; i++) {

                if (trackers[i].get('trackingId') === config.gaid) {

                    var pageView = 'pageview',
                        tracker = trackers[i];

                    if (tracker) {

                        args.shift();

                        var extra = args.length > 0 && args[0] !== undefined && args[0] !== pageView ? args[1] : '';

                        var hitType = args.length && args[0] !== undefined && args[0] === pageView ? args[0] : 'event',
                            category =  config.category,
                            action =  config.testId + ' - ' + config.devices + ' - ' + config.page + ' - ' + config.action + ' - ' + config.variant + ' | ' + args[0] ,
                            label = args.length > 2 && args[1] !== undefined && typeof args[1] !== 'boolean' ? extra : '',
                            nonInteraction = args.length && args[ args.length - 1 ] !== undefined && typeof args[ args.length - 1 ] === 'boolean' ? args[ args.length - 1 ] : true
            
                        if (hitType === pageView) {

                            tracker.send.apply(tracker, [
                                hitType
                            ]);
                        
                        } else {

                            tracker.send.apply(tracker, [
                                hitType, 
                                category, 
                                action, 
                                label,
                                nonInteraction
                            ]);
                        
                        }
                    }

                    break;
                
                } else if (i === trackers.length - 1) {

                    if (trackerIndex !== 500) {

                        console.warn('Tracker not found, Trying again');

                        setTimeout(function () {

                            gaTracker();
                        
                        }, 50);

                        trackerIndex++;
                    
                    } else {

                        console.error("TRACKER: \"".concat(config.gaid, "\" NOT FOUND"));
                    
                    }
                }
            }
        } else if (GAindex != 500) {

            setTimeout(function () {

                gaTracker();

            }, 50);

            GAindex++;

        } else {

            console.error('Google Analytics not found!');

        }


    };

    gaTracker();

}