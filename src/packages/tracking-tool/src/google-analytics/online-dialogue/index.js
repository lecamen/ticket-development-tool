/*
* GA Tracking for Online Dialogue
*/

import gaTracker from "../tracker"

const gaSend = function( config ) {

    const args = [
        ...arguments
    ]

    gaTracker( config, tracker => {

        const pageView = 'pageview'

        if ( tracker ) {

            args.shift()

            let extra = args.length > 0 && args[0] !== undefined && args[0] !== pageView ? args[1] : '';

            let hitType = args.length && args[0] !== undefined && args[0] === pageView ? args[0] : 'event',
                category =  config.category,
                action =  config.testId + ' - ' + config.devices + ' - ' + config.page + ' - ' + config.action + ' - ' + config.variant + ' | ' + args[0] ,
                label = args.length > 2 && args[1] !== undefined && typeof args[1] !== 'boolean' ? extra : '',
                nonInteraction = args.length && args[ args.length - 1 ] !== undefined && typeof args[ args.length - 1 ] === 'boolean' ? args[ args.length - 1 ] : true

            if ( hitType === pageView ) {

                tracker.send.apply( tracker, [
                    hitType
                ])

            } else {
                
                tracker.send.apply( tracker, [
                    hitType,
                    category,
                    action,
                    label, 
                    {
                        nonInteraction: nonInteraction
                    }
                ])

            }

        }

    })

}

export default gaSend