/*
* GA Conversion Tracking
*/

import gaTracker from "../tracker";

const gaSend = function( config ) {

    const args = [
        ...arguments
    ]

    gaTracker( config, tracker => {
        
        if ( tracker ) {

            const CLIENT_DOMAIN = `conversion.com`
            const CLIENT_CODE = config.options.CLIENT_CODE;
            const EXPERIMENT_NO = config.options.EXPERIMENT_NO;
            const VARIATION_NO = config.options.VARIATION_NO;
            const EXPERIMENT_ID = `CONV_EXPERIMENT=${EXPERIMENT_NO}`;
            const SELECTOR_PREFIX = `conv-${CLIENT_CODE.toLowerCase()}-${EXPERIMENT_NO.replace('.', '-')}`;
            const PREVIEW_TOKEN_NAME = 'CONV_EXPERIMENT';
            const PREVIEW_ACTIVE = (document.cookie.indexOf(PREVIEW_TOKEN_NAME) >= 0 && document.cookie.match(new RegExp(`(^| )${PREVIEW_TOKEN_NAME}=([^;]*)`, 'i'))[2].includes(EXPERIMENT_NO)) || (window.sessionStorage.getItem(PREVIEW_TOKEN_NAME) && window.sessionStorage.getItem(PREVIEW_TOKEN_NAME).split(',').includes(EXPERIMENT_NO));
            const DEBUG_MODE_TOKEN_NAME = 'CONV_DEBUG_MODE';
            const DEBUG_MODE_ACTIVE = PREVIEW_ACTIVE || document.cookie.indexOf(DEBUG_MODE_TOKEN_NAME + '=enabled') >= 0 || window.sessionStorage.getItem(DEBUG_MODE_TOKEN_NAME) === 'enabled';
            const DEBUG_PREFIX = `[CONV] ${(PREVIEW_ACTIVE ? '[QA]' : '[DEV]')} ${CLIENT_CODE} ${EXPERIMENT_NO}`;

            args.shift()

            args.splice(1, 0, CLIENT_DOMAIN );

            args.splice(2, 0, `${ CLIENT_CODE } ${ EXPERIMENT_NO } - ${ (!VARIATION_NO || VARIATION_NO == '0' ? 'Control' : `Variation ${VARIATION_NO}`) }`);

            try {

				if (DEBUG_MODE_ACTIVE && !document.title.indexOf(DEBUG_PREFIX) >= 0) {

                    DEBUG_MODE_ACTIVE && console.log(`${DEBUG_PREFIX} ${(!VARIATION_NO || VARIATION_NO == '0' ? 'Control' : `Variation ${VARIATION_NO}`)} -->`, ...args)

                    document.title = `${DEBUG_PREFIX} ${document.title}`
				
                }

			} catch (error) {

				console.log(error.message)

			}

            tracker.send.apply( tracker, args )

        }

    })

}

export default gaSend