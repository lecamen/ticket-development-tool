const gaTracker = ( config, callback ) => {

    let trackerIndex = 0,
        GAindex = 0

    if ( !config || ( config && !'gaid' in config ) ) {
        
        console.log('Please check tool configuration')
        
        return

    }

    if ( window[window['GoogleAnalyticsObject']] && window[window['GoogleAnalyticsObject']].create ) {
 
        let allTrackers = window[window['GoogleAnalyticsObject']].getAll(),
            i

        for (i = 0; i < allTrackers.length; i++) {
           
            if ( allTrackers[i].get('trackingId') === config.gaid ) {
                
                callback( allTrackers[i] )

                break

            } else if  ( i === allTrackers.length - 1 ) {
                
                if (trackerIndex !== 500) {

                    console.warn('Tracker not found, Trying again')

                    setTimeout( () => {

                        gaTracker( config, callback )

                    }, 50)

                    trackerIndex++

                } else {

                    console.error("TRACKER: \"".concat(config.gaid, "\" NOT FOUND"))

                }
            }
        }

    } else if (GAindex != 500) {

        setTimeout( () => {

            gaTracker( config, callback )

        }, 50)

        GAindex++

    } else {

        console.error('Google Analytics not found!')

    }
}

export default gaTracker
