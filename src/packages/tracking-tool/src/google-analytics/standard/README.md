# Standard GA Tracking #

This method send data to Google Analytics ( Standard ).

---

**How do I get set up?**

```jsx

// Import from specific directory.

import gaSend from './[DIR]/google-analytics/standard';

// if not using node module 
// copy snippet from /[DIR]/google-analytic/standard/es5/google-analytic.es5.min.js

```

**Configurations**

```jsx
/*
* GA Config
*/
var toolConfig = {
    gaid: 'UA-202XXXX-XX'
}

/*
* If fields object needed as global
* e.g 'dimension15':  'My Custom Dimension'
* https://developers.google.com/analytics/devguides/collection/analyticsjs/custom-dims-mets#:~:text=Custom%20dimensions%20and%20metrics%20are%20a%20powerful%20way,other%20business%20data%20you%20have%20on%20a%20page.
*/
var toolConfig = {
    gaid: 'UA-202XXXX-XX',
	fieldsObject: {
		dimension15:  'My Custom Dimension',
		nonInteraction: false // TRUE or FALSE
	}
}
```

**Parameters**

```jsx
/*
* @config
* Tool configuration
*/

/*
* @event : e.g "event", "set" or "pageview"
* @eventCategory : e.g "Category"
* @eventAction: e.g "Custom action event"
* @eventLabel : e.g "Custom label event"
* @eventValue : e.g "Custom value event"
* @fieldsObject : e.g { nonInteraction: true }
* https://developers.google.com/analytics/devguides/collection/analyticsjs/sending-hits#:~:text=ga%28%27send%27%2C%20%27pageview%27%29%3B%20The%20object%20that%20is%20doing%20the,explains%20how%20to%20control%20what%20data%20gets%20sent.
*/

/*
* @nonInteraction as fieldsObject
* https://developers.google.com/analytics/devguides/collection/analyticsjs/events?hl=en 
*/

/*
* https://developers.google.com/analytics/devguides/collection/analyticsjs/custom-dims-mets#:~:text=Custom%20dimensions%20and%20metrics%20are%20a%20powerful%20way,other%20business%20data%20you%20have%20on%20a%20page.
*/

gaSend(toolConfig, 'event', eventCategory, eventAction, eventLabel, eventValue, { nonInteraction: false })

gaSend(toolConfig, 'event', eventCategory, { nonInteraction: true })

/*
* Use global fieldsObject
*/
gaSend(toolConfig, 'event', eventCategory, eventAction, eventLabel, eventValue, fieldsObject )

gaSend(toolConfig, 'event', eventCategory, fieldsObject )
```

**Usage**

```jsx
/*
* GA Config
* See more for global config fields object above
*/
var toolConfig = {
    gaid: 'UA-202XXXX-XX'
}

/* GA Send */
function gaSend(o){ ... }
/* END - GA Send */

/*
* Sample click event
*/
document.body.addEventListener('click', function(){

	gaSend(toolConfig, 'event', 'eventCategory', 'eventAction', 'eventLabel', 'eventValue', { nonInteraction: false } )

})

/*
* Sample Page view
*/
gaSend(toolConfig, 'pageview',  { nonInteraction: true })

```