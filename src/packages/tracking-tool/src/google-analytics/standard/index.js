/*
* GA Standard Tracking
*/

import gaTracker from "../tracker";

const gaSend = function( config ) {

    const args = [
        ...arguments
    ]

    gaTracker( config, tracker => {
        
        if ( tracker ) {

            args.shift()

            tracker.send.apply( tracker, args )

        }

    })

}

export default gaSend