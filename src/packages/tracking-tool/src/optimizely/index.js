/*
* Optimizely Tracking
*/

const optimizelySend = ( data ) => {

    window.optimizely = window.optimizely || []

    window.optimizely.push( data )

}

export default optimizelySend