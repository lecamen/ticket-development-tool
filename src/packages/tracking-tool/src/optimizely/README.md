# Optimizely Tracking #

This method send data using optimizely custom event. 

---

**How do I get set up?**

```jsx

// Import from specific directory.

import optimizelySend from './[DIR]/optimizely';

// if not using node module 
// copy snippet from /[DIR]/optimizely/es5/optimizely.es5.min.js

```

## Usage

```jsx
/*
* For Optimizely X
* https://help.optimizely.com/Build_Campaigns_and_Experiments/Custom_events_in_Optimizely_X
* https://help.optimizely.com/Measure_success%3A_Track_visitor_behaviors/Set_up_revenue_tracking_in_Optimizely_X_Web
*/

// Sends a tracking call to Optimizely for the given event name. 
optimizelySend({
   type: "event",
   eventName: "eventName"
})

//Implement the Optimizely revenue tracking code
optimizelySend({
    "type": "event",
    "eventName": "trackRevenue",
    "tags": {
        "revenue": 1.50
    }
})

```

```jsx
/*
* For Optimizely Classic
*/

optimizelyClassicSend([
    "trackEvent", 'eventName'
]);
```