import gaSend from "./google-analytics/custom"
 
var toolConfig = {
    gaid: 'UA-49794614-1',
    testId: 'OD235',
    category: 'Video Playback',
    variant: 'B: Variant'
};
 
gaSend(toolConfig, 'pageview')

document.body.addEventListener('click', () => {
 
    gaSend(toolConfig, 'Action : User click CTA',  { nonInteraction: false } )

    // Extra
    gaSend(toolConfig, 'Action : User click CTA', 'Extra: Label...', { nonInteraction: false } )

})