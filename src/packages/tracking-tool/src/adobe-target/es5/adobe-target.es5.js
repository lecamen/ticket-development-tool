/*
* Adobe Traget Tracking
*/

function adobeTargetSend(data, extra) {

    if ( typeof s.tl !== 'undefined' ) {
  
      	s = !extra ? s : Object.assign({}, s, extra);
  
      	s.tl(true, 'o', data);
    
    }

}