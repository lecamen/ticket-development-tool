const path = require('path');

module.exports = {
    mode: 'production',
    entry: {
        'ga-online-dialogue-test' : './src/ga-online-dialogue-test',
        'ga-conversion-test' : './src/ga-conversion-test',
        'ga-standard-test' : './src/ga-standard-test',
        'ga-custom-test' : './src/ga-custom-test',
        'convert-test' : './src/convert-test'
    },
    watch: true,
    module: {
    },
    output: {
        clean: true,
        path: path.resolve(__dirname, 'dist'),
    }
};
