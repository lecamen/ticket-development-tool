import AntiFlicker from "../packages/anti-flicker"

import ready from "./ready"

/*
* Development Bootstrap
*/

const bootstrap = ( config, callback ) => {
    
    localStorage.setItem(`tc[${ config.ID}-${ config.VARIANT }]`, 'dev')

    const storage = localStorage.getItem(`tc[${ config.ID}-${ config.VARIANT }]`)

    if ( storage && storage === 'dev' ) {

        config.LIVE = storage
        
        window.__TC_EXPERIENCE__ = window.__TC_EXPERIENCE__ || []

        __TC_EXPERIENCE__.push( config )

        if ( config.ANTI_FLICKER ) {

           AntiFlicker.mounted( config.ANTI_FLICKER )

        }

        if ( config.READY ) {

            ready( () => {

                callback()

            })


        } else {

            callback()
            
        }

    }

}

export default bootstrap