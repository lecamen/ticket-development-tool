
import pushStateChange from "../../../helpers/push-state-change"

import ajaxComplete from "../../../helpers/ajax-complete"

/*
* Warning!!!!!!
* Don't removed __IMPORTS__! This is a part of webpack loader
*/

'__IMPORTS__'

export default {

    /*
    * Native: Query one element
    */
    get querySelector () {

        return document.querySelector.bind( document )

    },

    /*
    * Native: Query all element
    */
    get querySelectorAll () {

        return document.querySelectorAll.bind( document )

    },

    /*
    * Native: create element
    */
    get createElement () {
        
        return document.createElement.bind( document )

    },

    /*
    * Set Language value
    * Useful for SPA website
    */

    set language ( value ) {

        this._language = value

    },

    /*
    * Return page language
    */
    get language () {

        let lang = this.querySelector('html').getAttribute('lang') || this.config.FORCE_LANGUAGE || 'en'

        if ( lang.includes('-') ) {

            lang = lang.split('-')[0]

        }

        return this._language || lang.toLowerCase()

    },
    
    /*
    * Set translation values
    */
    get translation () {
        
        return this._translation

    },

    /*
    * Set translation values
    */
    set translation ( value ) {

        this._translation = value[ this.language ]

    },

    /*
    * Get configuration values
    */
    get config () {

        return this._config

    },

    /*
    * Set configuration values
    */
    set config ( value ) {

        this._config = value

    },

    /*
    * This will return a ticket identity with prefix
    * @prefix : attach to ticket indentity
    */
    identity ( prefix ) {

        return `${ prefix }-${ this.config.IDENTITY }`.toLowerCase()

    },

    /*
    * This will return the id of the ticket
    */
    get id () {

        return this.config.ID

    },
    
    /*
    * This will return the variant of the ticket
    */
    get variant () {

        return this.config.VARIANT

    },

    /*
    * Ticket domain name. 
    * This is required if the Ticket need amazon s3 assets hosting.
    * https://ta-client-assets.s3.amazonaws.com/[domain-name]/[ticket-id]/[filename.svg]'
    */
    s3( image ) {

        return 'https://ta-client-assets.s3.amazonaws.com' + '/' + this.config.DOMAIN_NAME.toLowerCase() + '/' + this.config.ID + '/' + image

    },

    /*
    * This will add a style element of the ticket and load to head
    */
    addStyle () {

        const key = this.identity('tcs')

        const $head = this.querySelector('head'),
            $style = this.querySelector(`#${ key }`)

        const $element = this.createElement('style')

        if ( !$style ) {

            $element.id = key
            $element.setAttribute('type', 'text/css')
            $element.innerHTML = this.style.toString().trim()

            $head.append( $element )

        }

    },

    /*
    * This will add identity of the ticket to html tag
    */
    addId () {

        const key = this.identity('tcv')

        const $head = this.querySelector('html')

        if ( $head && !$head.classList.contains(key) ) {

            $head.classList.add(key)

        }

    },

    /*
    * This will remove identity of the ticket in html tag
    * It's useful for SPA site
    */
    removeId () {

        const key = this.identity('tcv')

        const $html = this.querySelector('html')

        if ( $html && $html.classList.contains(key) ) {

            $html.classList.remove(key)

        }

    },

    /*
    * This will remove style of the ticket in head tag
    * It's useful for SPA site
    */
    removeStyle() {

        const key = this.identity('tcs')

        const $style = this.querySelector(`#${ key }`)

        if ( $style ) {

            $style.remove()

        }

    },

    /*
    * Initialize variant 
    */
    init () {

        this.config.DOMAIN_NAME = this.config.DOMAIN_NAME.toLowerCase()
        this.config.VARIANT = this.config.VARIANT.toLowerCase()
        this.config.IDENTITY = `${ this.config.ID.toString() }-${ this.config.VARIANT }`

        this.addId()

        this.addStyle()

        /*
        * Warning!!!!!!
        * Don't removed __APPS__! This is a part of webpack loader
        */

        const load = ['__APPS__']

        let psc = 0,
            ops = 0

        const loadApp = load.map( app => {

            const _this = this

            return Object.assign( app, _this )

        })

        loadApp.forEach( app => {

            if ( app.mounted ) {

                app.mounted()

            }

        })

        ajaxComplete ( (self, args) => {

            loadApp.forEach( app => {

                if ( app.completed ) {

                    app.completed( self, args )

                }

            })
           
        })

        pushStateChange( ( _history, _args ) => {

            clearTimeout( psc )
            
            psc = setTimeout( () => {

                loadApp.forEach( app => {

                    if ( app.updated ) {
    
                        app.updated( _history, _args )
    
                    }
    
                })

            }, 300)

        })

        window.onresize = function ( event ) {

            clearTimeout( ops )

            ops = setTimeout( () => {

                loadApp.forEach( app => {

                    if ( app.updated ) {
    
                        app.updated( event )
    
                    }
    
                })

            }, 300)

        }

        window.onpopstate = function ( event ) {

            clearTimeout( ops )

            ops = setTimeout( () => {

                loadApp.forEach( app => {

                    if ( app.updated ) {
    
                        app.updated( event )
    
                    }
    
                })

            }, 300)

        }
    
    }
    
}