/*
* You can import tracking tool from "./src/packages/tracking-tool"
* e.g
* import gaSend from "../../../../packages/tracking-tool/src/google-analytics/online-dialogue"
*/

export default {

    mounted () {
        
        /*
        * Entry Point (Immediately or Ready)
        * This will trigger Immediately or Ready
        * Or load Mutation Observer here
        */

    },

    updated () {

        /*
        * Entry Point (Push State and Postate)
        * This will trigger when Push State and Postate changed
        * Recommend to use for SPA site
        */

    },

    completed () {

        /*
        * Entry Point (XHR) Complete
        * This will trigger when XHR request completed
        */

    }

}