/*
* Warning!!!!!!
* Don't removed __BOOTSTRAP__! This is a part of webpack loader
*/

'__BOOTSTRAP__'

import core from './core/'

import config from './config'
import style from './css/style.css'

/*
* Languages
*/
import en from './lang/en.json'

/*
* Run variant
*/
bootstrap( config, () => {

    core.config = config
    
    core.style = style

    /*
    * Change translation if needed
    */
    core.translation =  {
        en: en
    }

    core.init()

})