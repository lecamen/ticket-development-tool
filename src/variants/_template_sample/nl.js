/*
* Warning!!!!!!
* Don't removed __BOOTSTRAP__! This is a part of webpack loader
*/

'__BOOTSTRAP__'

import core from './core/'

import config from './config'
import style from './css/style.css'

/*
* Languages
*/
import nl from './lang/nl.json'

/*
* Run variant
*/
bootstrap( config, () => {

    core.config = config
    
    core.style = style

    /*
    * Change translation if needed
    */
    core.translation =  {
        nl: nl
    }

    core.init()

})