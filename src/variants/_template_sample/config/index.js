/*
* Ticket VERSION
*
* Warning!!!!!!
* Don't removed __VERSION__! This is a part of webpack loader
*/

exports.VERSION = '__VERSION__'


/*
* Ticket NUMBER or ID
*/
exports.ID = 1000

/*
* Ticket variation reference
*/
exports.VARIANT = 'b'

/*
* Ticket name or title
* Ticket ID should excluded
*/
exports.NAME = 'Ticket Name'

/*
* Ticket domain name. 
* This is required if the Ticket need amazon s3 assets hosting.
* https://ta-client-assets.s3.amazonaws.com/[domain-name]/[ticket-id]/[filename.svg]'
*/
exports.DOMAIN_NAME = 'teamcroco'

/*
* Ticket Build Prefix
* Default Prefix - cro
* Use variant e.g color blue or orange, small or big
* eports.PREFIX = 'blue'
*/
exports.PREFIX = 'cro'

/*
* Run experiment IMMEDIATELY or READY
* If TRUE, expermement will load when document is ready
* If FALSE, expermement will load immediately
*/
exports.READY = false

/*
* Run experiment with Anti-Flicker default 0 second delay
*/
exports.ANTI_FLICKER = 0

/*
* Run this selected language.
* This only work if no default language found,
*/
exports.FORCE_LANGUAGE = 'en'

/*
* Load, Ready or SPA Target
*/

exports.TARGET_PATH = [
]

/*
* Load, Ready or SPA Exclude Target
*/
exports.EXCLUDE_PATH = [
]