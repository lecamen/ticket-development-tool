import requestProxy from  "../../../../helpers/proxy-me";

export default {

    mounted() {

        requestProxy({
            url: 'https://google.com',
            params: {
                p1: 1,
                p2: 2
            }
        }, response => {

            console.log('response: ', response)

        })
        
    }

}