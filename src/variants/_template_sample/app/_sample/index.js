import image from "../../../../images/image.png"

const asset = {
    image: image
}

export default {

    spa () {

        return this.translation.spa

    },

    hello () {

        return this.translation.hello

    },

    mounted () {
        
        /*
        * Entry Point (Immediately or Ready)
        * This will trigger Immediately or Ready
        * Or load Mutation Observer here
        */

        const say = this.hello()
    
        const $img = this.createElement('img')
        
        $img.src = asset.image
    
        document.body.prepend(
            $img
        )
    
        console.log( say )

    },

    updated () {

        /*
        * Entry Point (Push State and Postate)
        * This will trigger when Push State and Postate changed
        * Recommend to use for SPA site
        */

    },

    completed () {

        /*
        * Entry Point (XHR) Complete
        * This will trigger when XHR request completed
        */

    }
    

}