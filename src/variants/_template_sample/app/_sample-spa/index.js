const targetUrls = [
    '/'
]

export default {

    spa () {

        return this.translation.spa

    },

    hello () {

        return this.translation.hello

    },

    mounted () {

        // This will trigger Immediately or Ready
        // Or load Mutation Observer here

        console.log( this.hello( this.translation ) + ' In SPA sample!')

    },

    updated () {

        // This will trigger when Push State and Popstate changed
        // Recommend to use for SPA site

        const say = this.spa()

        if ( targetUrls.includes('/') ) {

            /*
            * If valid run your custom funtion/method
            */
            console.log( say )

        } else {

            /*
            * If valid run your custom funtion/method
            */

            console.log( 'Hide/remove any variant/apps belong in test!' )

        }


    },

    completed () {

        // This will trigger when XHR request completed

    }
    

}