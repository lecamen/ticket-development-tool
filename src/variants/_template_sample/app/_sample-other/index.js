import otherFunction from "./other-function"

import image from "../../../../images/image.png"

const asset = {
    image: image
}

const customFunction = ( translation ) => {

}

const customEvent = () => {
    
}

export default {

    hello () {

        console.log( asset.image )

    },

    mounted () {
        
        /*
        * Entry Point (Immediately or Ready)
        * This will trigger Immediately or Ready
        * Or load Mutation Observer here
        */

        /*
        * Merge "otherFunction" Objects
        * Careful when merging object. It will override the same methods name
        */
        Object.assign( this, otherFunction)

        this.hello()

        this.other()

        customFunction( this.translation )

        customEvent()

    },

    updated () {

        /*
        * Entry Point (Push State and Postate)
        * This will trigger when Push State and Postate changed
        * Recommend to use for SPA site
        */

    },

    completed () {

        /*
        * Entry Point (XHR) Complete
        * This will trigger when XHR request completed
        */

    }

}