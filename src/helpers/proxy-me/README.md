# Proxy Me

You can simply use this website as quickest way to start doing some cross-domain requests. 
The CORS proxy service expects you provide the URL service/page in url HTTP GET/POST parameter.

---

**How do I get set up?**


```jsx

// Import from specific directory.

import requestProxy from  "../[DIR]/proxy-me";


// if not using node module 
// copy snippet from /[DIR]/proxy-me/es5/index.min.js

```


---


**Parameters**

```jxs

@url : Cross domain url e.g (https://fromdomain.com)
@params : An extra parameters if require from cross domain url. e.g params=[param=1, param=2];

```


---


**Usage**

No params from cross domain
```jsx

requestProxy({
    url: 'https://domain.com'
}, response => {

    console.log('response: ', response)

})

```

---


With extra params from cross domain

```jsx

requestProxy({
    url: 'https://domain.com',
    params: {
        p1: true,
        p2: 'Hello'
    }
}, response => {

    console.log('response: ', response)

})

```


