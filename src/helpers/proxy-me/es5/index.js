function concatParams(params) {

    var str = '';

    for (var i in params) {

        str += (str !== '' ? ',' : '') + i + '=' + params[i];

    }

    str = '[' + str + ']';

    return str;

}

function requestProxy(options, callback) {

    var host = 'https://puddle.teamcroco.com/cors/proxy-me.php',
        key = 'd41d8cd98f00b204e9800998ecf8427e',
        origin = location.origin,
        url = options.url || null,
        params = options.params || null;

    var requestUrl = host + '?key=' + key + '&origin=' + origin + '&url=' + url + (params ? '&params=' + concatParams(params) : '');

    ajaxRequest({
        url: requestUrl,
        method: options.method || 'GET',
        data: options.data || {}
    }, function (response) {

        callback(response);

    });
    
}