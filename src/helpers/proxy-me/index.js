import ajaxRequest from "../ajax-request";

const concatParams = ( params ) => {

    let str = ''

    for ( const i in params ) {

        str += (str !== '' ? ',' : '') + i + '=' + params[i]

    }

    str = '[' + str + ']'

    return str

}

const requestProxy = ( options, callback ) => {

    const host = 'https://puddle.teamcroco.com/cors/proxy-me.php',
        key = 'd41d8cd98f00b204e9800998ecf8427e',
        origin = location.origin,
        url = options.url || null,
        params = options.params || null

    const requestUrl = host + '?key=' + key + '&origin=' + origin + '&url=' + url + ( params ? '&params=' + concatParams(params) : '' )

    ajaxRequest({
        url: requestUrl,
        method: options.method || 'GET',
        data: options.data || {}
    }, response => {

        callback( response )

    })

}

export default requestProxy