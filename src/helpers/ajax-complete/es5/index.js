function ajaxComplete(callback) {

    var send = XMLHttpRequest.prototype.send;

    XMLHttpRequest.prototype.send = function () {

        this.addEventListener('load', function() {

            callback( this, arguments )

        })

        return send.apply( this, arguments )
    
    };
    
}