# Ajax Complete

This will fired any of the XHR state completed and callback.

---

**How do I get set up?**


```jsx

// Import from specific directory.

import ajaxComplete from './[DIR]/ajax-complete';

// if not using node module 
// copy snippet from /[DIR]/ajax-complete/es5/index.min.js

```


---


**Parameters**

```jxs

@callback : self or this and arguments


```

---


**Usage**


```jsx

// GET
ajaxComplete(function(self, args){

    // Todo 

})

```