function simplifiedPrice(value) {

    if (value !== '') {

        value = value.replace(/€/g, '').replace(/,/g, '.');
        value = parseFloat(value).toFixed(2);

        if (value.indexOf('00') !== -1) {

            value = value.replace('00', '-');
            
        }

        if (value.indexOf('.') !== -1) {

            value = value.replace('.', ',');

        }

        return value;

    }

    return value;

}