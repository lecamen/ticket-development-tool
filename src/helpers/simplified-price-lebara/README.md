# Simplified Price Lebara

Simplified Price for Lebara

---

**How do I get set up?**


```jsx

// Import from specific directory.

import simplifiedPrice from './[DIR]/simplified-price-lebara';

// if not using node module 
// copy snippet from /[DIR]/simplified-price-lebara/es5/index.min.js

```


---


**Parameters**

```jxs

@value : the price value

```


---


**Usage**


```jsx

const currentPrice = '€5.25'
const newPrice = simplifiedPrice(currentPrice)

// Output 5.25

const currentPrice = '€5.00'
const newPrice = simplifiedPrice(currentPrice)

// Output 5,-

```