/*
* checkin if device is a mobile device or tablet
*/

const isMobileOrTablet = (  ) => {
             
    var match = window.matchMedia || window.msMatchMedia;
    if(match) {
        var mq = match("(pointer:coarse)");
        return mq.matches;
    }
    return false;

}

export default isMobileOrTablet
