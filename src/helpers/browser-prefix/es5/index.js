function browserPrefix(property, value, isValue) {

    var prefix = ['-webkit-', '-moz-', '-ms-', '-o-', ''];

    var css = '';

    for (var i = 0; i < prefix.length; i++) {

        if (isValue) {

            css += ' ' + property + ': ' + (prefix[i] + value) + ';';
            
        } else {

            css += ' ' + (prefix[i] + property) + ': ' + value + ';';

        }
    }

    return css;

}