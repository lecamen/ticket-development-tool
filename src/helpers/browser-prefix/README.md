# Browser Prefix

Add prefix browser to styling.

---

**How do I get set up?**


```jsx

// Import from specific directory.

import browserPrefix from './[DIR]/browser-prefix';

// if not using node module 
// copy snippet from /[DIR]/browser-prefix/es5/index.min.js

```


---


**Parameters**

```jxs

@property : e.g transform
@value : e.g translate(-50%, -50%)

```


---


**Usage**


```jsx

// if property prefix
browserPrefix('transform', 'translate(-50%, -50%)')

// if value prefix set to true
browserPrefix('display', 'flex', true)

```


---


**Output**

```jsx
// if property prefix
-webkit-border-radius: 0 10px 0 10px;
-moz-border-radius: 0 10px 0 10px; 
-ms-border-radius: 0 10px 0 10px; 
-o-border-radius: 0 10px 0 10px; 
border-radius: 0 10px 0 10px;

// if value prefix
display: -moz-flex;
display: -ms-flex;
display: -webkit-flex; 
display: flex;
```