const browserPrefix = ( property, value, isValue ) => {
    
	const prefix = [
        '-webkit-',
        '-moz-', 
        '-ms-', 
        '-o-', 
        ''
    ]
	
    let css = ''

	for ( let i = 0; i < prefix.length; i++ ) {
	
        if ( isValue ) {

            css += ` ${ property }: ${ prefix[i] + value };`

        } else {
            
            css += ` ${ prefix[i] + property }: ${ value };`

        }
	
    }
	
    return css

}

export default browserPrefix