# Native JS

Use custom menthod name bind in document.

---

**How do I get set up?**


```jsx

// Import from specific directory.

import { querySelector, querySelectorAll, createElement } from './[DIR]/native-js';

import { querySelector as qs, querySelectorAll as qsa, createElement as ce } from './[DIR]/native-js';

// if not using node module 
// copy snippet from /[DIR]/native-js/es5/index.js

```

---


**Usage**


```jsx

// Query One Element
querySelector('body')

// Query All Elements
querySelectorAll('li')

// Create Element
createElement('div')

```