const querySelector = document.querySelector.bind(document)

const querySelectorAll = document.querySelectorAll.bind(document)

const createElement = document.createElement.bind(document)

export { querySelector, querySelectorAll, createElement }