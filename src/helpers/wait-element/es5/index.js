function waitElement(selector, callback) {

    var attempt = 0;

    function init() {

        var $element = document.querySelectorAll(selector);

        if ($element.length) {

            callback($element);
            
        } else {

            if (attempt <= 300) {

                setTimeout(function () {

                    init();

                    attempt++;

                }, 60);
            }
        }
    }

    init();

}