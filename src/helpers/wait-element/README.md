# Watch Element

This will check and wait the element. The element is delayed or fired by XHR.

---

**How do I get set up?**


```jsx

// Import from specific directory.

import waitElement from './[DIR]/wait-element';

// if not using node module 
// copy snippet from /[DIR]/wait-element/es5/index.min.js

```


---


**Parameters**

```jxs

@selector : a target element selector
@callback : a callback function

```


---


**Usage**


```jsx

waitElement('.element', function(){

	// Todo

})

```