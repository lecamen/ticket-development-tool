/*
* Find and wait selected element.
* @selector : target element selector
* @callback : fires when element is found and return element nodelist
*/

let attempt = 0

const waitElement = ( selector, callback ) => {

    const $element = document.querySelectorAll( selector )

	if ( $element.length ) {
		
        callback( $element )
	
    } else {
	
        if ( attempt <= 300 ) {

            setTimeout( () => {
            
                waitElement( selector, callback )

                attempt++
            
            }, 60)

        }
	
    }

}

export default waitElement