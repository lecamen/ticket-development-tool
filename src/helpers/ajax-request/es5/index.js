function ajaxRequest(options, callback) {

    options.method = options.method || "GET";
    options.headers = options.headers || [];
    options.data = options.data || {};

    var xhr = new XMLHttpRequest();

    xhr.withCredentials = true;
    xhr.responseType = options.responseType || '';

    xhr.onreadystatechange = function () {

        callback(this);
        
    };

    xhr.open(options.method, options.url);

    options.headers.forEach(function (header) {

        xhr.setRequestHeader(header.name, header.value);

    });

    if (options.method !== "GET") {

        xhr.send(JSON.stringify(options.data));

    } else {

        xhr.send();

    }
}