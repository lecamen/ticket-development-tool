# Ajax Request

You can post or retrieve data from a URL without having to do a full page refresh.

---

**How do I get set up?**


```jsx

// Import from specific directory.

import ajaxRequest from './[DIR]/ajax-request';

// if not using node module 
// copy snippet from /[DIR]/ajax-request/es5/index.min.js

```


---


**Parameters**

```jxs

/*
* GET and POST XHR
*/

@options : object of headers, responseType, methods, url and data 

@callback : self or this


```


---

**Response Type**

We can use xhr.responseType property to set the response format:

"" (default) – get as string,
"text" – get as string,
"arraybuffer" – get as ArrayBuffer (for binary data, see chapter ArrayBuffer, binary arrays),
"blob" – get as Blob (for binary data, see chapter Blob),
"document" – get as XML document (can use XPath and other XML methods) or HTML document (based on the MIME type of the received data),
"json" – get as JSON (parsed automatically).
For example, let’s get the response as JSON:


---


**Usage**


```jsx

// GET
ajaxRequest({
    url: 'https://www.teamcroco.com'
}, function(self) {

    if (self.status === 200 ) {
        // Todo
    } else {
        // Error
    }

})

// GET with Header
ajaxRequest({
    url: 'https://www.teamcroco.com',
    method: "GET",
        headers: [
        {
            name: 'Content-type', 
            value: 'application/json; charset=utf-8'
        },
        ...,
        ...
    ]
}, function(self) {

    if (self.status === 200 ) {
        // Todo
    } else {
        // Error
    }

})

// POST with Header
let json = JSON.stringify({
  type: "A/B Testing",
  client: "Team Croco"
});

ajaxRequest({
    url: 'https://www.teamcroco.com/submit',
    method: "POST",
    data: json,
    headers: [
        {
            name: 'Content-type', 
            value: 'application/json; charset=utf-8'
        }
        ...,
        ...
    ]
}, function(self) {

    if (self.status === 200 ) {
        // Todo
    } else {
        // Error
    }

})

```