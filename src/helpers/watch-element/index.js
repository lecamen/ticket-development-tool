const watchElement = ( selector, callback, options ) => {
	
    const targetNode = document.querySelector(selector)

	const observer = new MutationObserver( function( mutationsList, observer ) {
	    
        callback.call(this, mutationsList, observer)

	})

	observer.observe(targetNode, options || { attributes: 1, childList: 1, subtree: 1 })

}

export default watchElement