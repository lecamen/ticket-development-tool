# Watch Element

This will observe selected element if there are changes.


---


**Warning**

Carefull of using MutationObserver it might break website functionalities.

The MutationObserver interface provides the ability to watch for changes being made to the DOM tree. It is designed as a replacement for the older Mutation Events feature, which was part of the DOM3 Events specification.

[https://developer.mozilla.org/en-US/docs/Web/API/MutationObserver](https://developer.mozilla.org/en-US/docs/Web/API/MutationObserver)

**How do I get set up?**


```jsx

// Import from specific directory.

import watchElement from './[DIR]/watch-element';

// if not using node module 
// copy snippet from /[DIR]/watch-element/es5/index.min.js

```


---


**Parameters**

```jxs

@selector : a target selector to observe

@callback : a callback function ( this, mutationsList, observer )



@options : configuration of the observer

    Default configuration
    {
        attributes: 1, 
        childList: 1, 
        subtree: 1
    }

```


---


**Usage**


```jsx

// Element to observe

// Default
watchElement('.element', function( self, mutationsList, observer ){

	// Todo
	dynamicContent()

})

```


---


```jsx
// With Options

const options = { 
	attributes: 0, 
	childList: 1, 
	subtree: 1 
}

watchElement('.element', function( self, mutationsList, observer ){

	// Todo
	dynamicContent();

}, options)

```