function watchElement(selector, callback, options) {

	var targetNode = document.querySelector(selector);

	var observer = new MutationObserver(function (mutationsList, observer) {

		callback.call(this, mutationsList, observer);
	
    });

	observer.observe(targetNode, options || { attributes: 1, childList: 1, subtree: 1 });

}