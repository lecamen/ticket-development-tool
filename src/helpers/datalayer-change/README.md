# Datalayer Change

This will check and wait the datalayer.

---

**How do I get set up?**


```jsx

// Import from specific directory.

import datalayerChange from './[DIR]/datalayer-change';

// if not using node module 
// copy snippet from /[DIR]/datalayer-change/es5/index.min.js

```


---


**Parameters**

```jxs

@event : a datalayer event
@callback : a callback function

```


---


**Usage**


```jsx

datalayerChange('optimize.domChange', function( data ) {

	// Todo

})

```