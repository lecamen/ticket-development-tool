
const waitUntil = (requirements, callback, intervalMs, timeoutMs) => {

    intervalMs = (typeof intervalMs !== 'undefined') ? intervalMs : 50;
    timeoutMs = (typeof timeoutMs !== 'undefined') ? timeoutMs : 10000;

    let interval;

    if (!interval) {

        interval = setInterval(function () {

            if (requirements()) {
                clearInterval(interval);
                interval = null;
                callback();
            }

        }, intervalMs);
    }
    setTimeout(function () {

        if (interval !== null) {
            
            clearInterval(interval);

        }
    }, timeoutMs);
}

const datalayerChange = ( event, callback ) => {

    waitUntil(function () {

        return typeof window.dataLayer !== 'undefined' && typeof window.dataLayer.push !== 'undefined'

    }, function () {

        const pushData = window.dataLayer.push

        window.dataLayer.push = function (data) {

            pushData.apply(this, arguments)

            if ( data.event === event || data[1] === event) {

                callback( data )

            }
        }
    })

}


export default datalayerChange