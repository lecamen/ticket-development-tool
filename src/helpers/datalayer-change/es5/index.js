var waitUntil = function waitUntil(requirements, callback, intervalMs, timeoutMs) {

    intervalMs = typeof intervalMs !== 'undefined' ? intervalMs : 50;
    timeoutMs = typeof timeoutMs !== 'undefined' ? timeoutMs : 10000;

    var interval = void 0;

    if (!interval) {

        interval = setInterval(function () {

            if (requirements()) {

                clearInterval(interval);
                interval = null;
                callback();

            }

        }, intervalMs);

    }
    setTimeout(function () {

        if (interval !== null) {

            clearInterval(interval);

        }

    }, timeoutMs);
};

var datalayerChange = function datalayerChange(event, callback) {

    waitUntil(function () {

        return typeof window.dataLayer !== 'undefined' && typeof window.dataLayer.push !== 'undefined';
        
    }, function () {

        var pushData = window.dataLayer.push;

        window.dataLayer.push = function (data) {

            pushData.apply(this, arguments);

            if (data.event === event || data[1] === event) {

                callback( data );

            }
        };
    });
};