function pushStateChange(callback) {

    var pushState = history.pushState;

    history.pushState = function (state) {

        callback(state, history);

        return pushState.apply(history, arguments);

    };

}