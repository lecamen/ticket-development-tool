const pushStateChange = function ( callback ) {

    const pushState = history.pushState
    
    history.pushState = function( state ) {
        
        callback( state, history )
        
        return pushState.apply( history, arguments )

    };

}

export default pushStateChange