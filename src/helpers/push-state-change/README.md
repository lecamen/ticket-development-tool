# Push State Chnage

In an HTML document, the history.pushState() method adds an entry to the browser's session history stack.

It's usefule when dealing SPA site.
---

**How do I get set up?**


```jsx

// Import from specific directory.

import pustStateChange from './[DIR]/push-state-change';

// if not using node module 
// copy snippet from /[DIR]/push-state-change/es5/index.min.js

```


---


**Parameters**

```jxs

@callback : fires when new state added

@callback -> state
@callback -> history (native)

```


---


**Usage**


```jsx

// SPA
pushStateChange(function( state, history ){
	// Todo
})

```