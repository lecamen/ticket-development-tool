const fieldClass = ( parentSelector, prefix ) => {

    document.querySelectorAll('[name]').forEach( $field => {

        const $parent = $field.closest( parentSelector )

        if ( $field && $parent) {

            prefix = prefix ? prefix : 'col'

            $parent.classList.add(
                `${ prefix }-${ $field.getAttribute('name') }`
            )

        }

    })

}

export default fieldClass