# Field Class

This will add class to all parent element of the input, select, texteare etc... 
The name of field is the class name of parent element.

---

**How do I get set up?**


```jsx

// Import from specific directory.

import fieldClass from './[DIR]/field-class';

// if not using node module 
// copy snippet from /[DIR]/field-class/es5/index.min.js

```


---

**Parameters**

```jxs

@parentSelector : the closest parent element selector 

@prefix : add prefix to class, default is "col"

```

---


**Usage**


```jsx

fieldClass('.col', 'field')

```