# Is Touch Device

Check for a touch screen device 

---

**How do I get set up?**


```jsx

// Import from specific directory.

import isTouchDevice from './[DIR]/is-touch-device';

// if not using node module 
// copy snippet from /[DIR]/is-touch-device/es5/index.js

```

---


**Usage**


```jsx

if ( isTouchDevice() {
    // Touch
} else {
    // Not Touch
}

```